<?php

define('THEME_URI', get_stylesheet_directory_uri());
define('CSS_URI', get_stylesheet_directory_uri() . '/assets/css');
define('JS_URI', get_stylesheet_directory_uri() . '/assets/js');

add_action('wp_enqueue_scripts', function() {
    // register une stylesheet
    wp_enqueue_style('main_css', THEME_URI . '/style.css');
    // register un script
    wp_enqueue_script('jquery', 'http://ajax.googleapis.com/ajax/libs/jquery/2.0.0/jquery.min.js', [], false, true);
    
    if(is_single()) {
        // ajouter le css de star rating
        wp_enqueue_style('star_rating_css', CSS_URI . '/star-rating.css');
        // ajouter le js de star rating
        wp_enqueue_script('star_rating_js', JS_URI . '/star-rating.js', ['jquery'], false, true);
    }
});

function get_movies_by_categories($categories, $id) {
    $args = [
        // type de post 
        'post_type' => 'movies',
        'post_status' => 'publish',
        'posts_per_page' => 4,

        'order_by' => 'post_date',
        'order' => 'desc',
        'post__not_in' => [$id]

        // 'meta_key' => 'rating',
        // 'order_by' => 'meta_value',
        // 'order' => 'desc'
    ];
    $tax_query = [
        'relation' => 'AND',
    ];
    $tax_query[] = [
        'taxonomy' => 'categories',
        'field' => 'term_id',
        'terms' => $categories,
        'operator' => 'IN'
    ];

    $args['tax_query'] = $tax_query;

    return new WP_Query($args);
}