<!-- lien vers la page de l'article courant -->
<a href="<?= get_the_permalink() ?>">
    <div class="card">
        <img src="<?= get_field('photo') ?>">
        <p><?= get_the_title() ?></p>
    </div>
<a>