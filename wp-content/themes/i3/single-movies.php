<?php $current_id = get_the_ID(); ?>

<?php get_header() ?>
    <div class="wrapper">
        <div class="movie-info">
            <h1><?= get_the_title() ?></h1>
            <div class="rating" data-rating="<?= get_field('rating') / 2 ?>"></div>
            <div style="display: flex;">
                <div class="movie-details">
                    <img src="<?= get_field('poster') ?>">
                    <p>Durée : <?= get_field('duree') ?></p>
                    <?php
                    // get all the categories of one movie
                        $tableau = get_the_terms(get_the_ID(), 'categories') ?: [];

                        array_map(function($x) { ?>

                            <div><?= $x->name ?></div>

                        <?php }, $tableau);?>
                </div>
                <div class="movie-synopsys">
                    <?= get_the_content() ?>
                </div>
            </div>
        </div>
        <div class="actors">
            <?php 
                $actors = get_field('acteurs') ?: [];
                usort($actors, function($a, $b) {
                    return get_field('nom', $a->ID) > get_field('nom', $b->ID);
                });
            ?>
            <?php foreach($actors as $post) {
                // DOIT s'appeler $post !!!!! WTF!!!
                // redéfini le post courant avec un acteur
                setup_postdata($post);
                get_template_part('template-parts/card', 'actor');
            } ?>
        </div>
    </div>
    <div class="movies-other">
        <h2>Autres films</h2>
        <?php $movies = get_movies_by_categories(array_map(function($x){
            return $x->term_id;
        }, $tableau), $current_id) ?>
        <?php while($movies->have_posts()) {
            $movies->the_post();
            echo '<p>' . get_the_title() . '</p>';
        } ?>
    </div>
<?php get_footer() ?>
<script>
    jQuery(".rating").starRating({
        readOnly: true
    });
</script>