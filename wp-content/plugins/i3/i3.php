<?php
/**
 * Plugin Name: Interface3
 * Author: Khun LY
 * Version: 0.0.1
 * Requires PHP: 7.2
 */

add_action('init', function() {

    // ajouter un nouveau type d'article dans wp
    register_post_type('movies', [
        'public' => true,
        'labels' => [
            'name' =>'Films',
            'singular_name' => 'Film'
        ]
    ]);

    register_post_type('actors', [
        'public' => true,
        'labels' => [
            'name' => 'Acteurs',
            'singular_name' => 'Acteur'
        ]
    ]);

    // ajouter un nouveau type de taxonomie dans wp
    register_taxonomy('countries', 'actors', [
        'public' => true,
        'labels' => [
            'name' => 'Pays',
            'singular_name' => 'Pays'
        ]
    ]);

    register_taxonomy('categories', 'movies', [
        'public' => true,
        'labels' => [
            'name' => 'Catégories',
            'singular_name' => 'Catégorie'
        ]
    ]);
});