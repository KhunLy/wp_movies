<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'wpmovies' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         '`pULwOjXVulG74cde|A{&9Pc[y!:aNK,&^$<dmmcR)^j2jL2hU@_dl.Gk^rpQ@b+' );
define( 'SECURE_AUTH_KEY',  'L{=ee^92%vZz(L!?U`b-sclg_Lqz!YyV^=TPNot`RUTE5=S96|fI)IT=2yIr`u!n' );
define( 'LOGGED_IN_KEY',    's<~Qjt@uLjj}3|=Uj44`^Em>X%(6McfXXM`ppmGt{9#3{:h<]d-RyL~I`.$5{PxH' );
define( 'NONCE_KEY',        ']K7i /Z>wvZ9JP&9]?KO>CR?p%)/<&LNjW/gmoj~Xt3:LnZ%{]nNd*rAk4PyGCQ@' );
define( 'AUTH_SALT',        'M{.Wa6b>:`,%ocBJmE,#Dd97.e;^.8,{f[DJz<u:F?ns=WpJj1)Eg?SR`|AgScV%' );
define( 'SECURE_AUTH_SALT', 'o7t7VePC1%C1Cufe<C?:BZ0x{W*zId.:{ri875XI 1fH8thq0:f&=jcR-Fs|A)Y5' );
define( 'LOGGED_IN_SALT',   'kcu3?S<z5mBr4[*%3 qBaOm]Su8Eac@v2cmXA4>I&xmz9K9-yV`0R.L?Ljz=G<l]' );
define( 'NONCE_SALT',       '^E|O.ZbN*$q-F^cZNYQHn]2xY^T58oW9HmdCQeL>cZzsYv8dPF2]e=++9@@N!;bc' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
